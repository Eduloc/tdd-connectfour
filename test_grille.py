import unittest

from grille import Grid

# Test de notre jeu du puissance 4

empty_grid = ".......\n.......\n.......\n.......\n.......\n.......\n"
grid_one_token = "O......\n.......\n.......\n.......\n.......\n.......\n"
grid_two_token_in_column = "O......\nO......\n.......\n.......\n.......\n.......\n"
grid_two_token_in_line = "OO.....\n.......\n.......\n.......\n.......\n.......\n"
grid_seven_token_in_column = "O......\nO......\nO......\nO......\nO......\nO......\n"

class GrilleTest(unittest.TestCase):
    def test_show_empty_grid(self):
        my_grid = Grid()
        self.assertEqual(my_grid.display(), empty_grid)

    def test_show_grid_after_one_token_insert(self):
        my_grid = Grid()
        my_grid.addToken(0)
        self.assertEqual(my_grid.display(), grid_one_token)

    def test_show_grid_after_adding_second_token_in_first_column(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(0)
        self.assertEqual(my_grid.display(), grid_two_token_in_column)

    def test_show_grid_after_adding_second_token_in_second_column(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(1)
        self.assertEqual(my_grid.display(), grid_two_token_in_line)

    def test_show_grid_after_adding_seven_token_in_first_column(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        self.assertEqual(my_grid.display(), grid_seven_token_in_column)

    def test_show_reset_grid(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.reset()
        self.assertEqual(my_grid.display(), empty_grid)

    def test_out_of_band_column(self):
        my_grid = Grid()
        my_grid.addToken(7)
        self.assertEqual(my_grid.display(), empty_grid)


if __name__ == '__main__':
    unittest.main()

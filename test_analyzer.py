import unittest

from analyzer import *
from grille import Grid


grid_full_line_started_fourth_column = "...OOOO\n.......\n.......\n.......\n.......\n.......\n"


class AnalyzerTest(unittest.TestCase):
    def test_player_win_first_line_first_column(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(1)
        my_grid.addToken(2)
        my_grid.addToken(3)
        self.assertEqual(line_win(my_grid), True)

    def test_player_win_first_line_fourth_column(self):
        my_grid = Grid()
        my_grid.addToken(3)
        my_grid.addToken(4)
        my_grid.addToken(5)
        my_grid.addToken(6)
        self.assertEqual(line_win(my_grid), True)

    def test_player_win_first_column_first_line(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        self.assertEqual(column_win(my_grid), True)

    def test_player_win_last_column_first_line(self):
        my_grid = Grid()
        my_grid.addToken(6)
        my_grid.addToken(6)
        my_grid.addToken(6)
        my_grid.addToken(6)
        self.assertEqual(column_win(my_grid), True)

    def test_player_win_right_diagonal(self):
        my_grid = Grid()
        my_grid.addToken(0)
        my_grid.addToken(1)
        my_grid.addToken(1)
        my_grid.addToken(2)
        my_grid.addToken(2)
        my_grid.addToken(2)
        my_grid.addToken(3)
        my_grid.addToken(3)
        my_grid.addToken(3)
        my_grid.addToken(3)
        my_grid.addToken(4)
        my_grid.addToken(4)
        my_grid.addToken(4)
        my_grid.addToken(4)
        my_grid.addToken(4)
        self.assertEqual(diagonal_right_win(my_grid), True)

    def test_player_win_left_diagonal(self):
        my_grid = Grid()
        my_grid.addToken(4)
        my_grid.addToken(3)
        my_grid.addToken(3)
        my_grid.addToken(2)
        my_grid.addToken(2)
        my_grid.addToken(2)
        my_grid.addToken(1)
        my_grid.addToken(1)
        my_grid.addToken(1)
        my_grid.addToken(1)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        my_grid.addToken(0)
        self.assertEqual(diagonal_left_win(my_grid), True)

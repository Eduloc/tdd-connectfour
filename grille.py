class Grid:
    def __init__(self):
        self.grid = ["."] * 42

    def get_grid(self):
        return self.grid

    def set_grid(self, grid):
        self.grid = grid

    def display(self):
        display_string = ""
        for lenght in range(6):
            for height in range(7):
                display_string += self.get_grid()[lenght * 7 + height]
            display_string += "\n"
        return display_string

    def addToken(self, column):
        if column < 7 :
            tmp_grid = self.get_grid()
            i = 0
            for lenght in range(6):
                if tmp_grid[lenght * 7 + column] != ".":
                    i += 1
            if i < 6:
                tmp_grid[i* 7 + column] = "O"
            self.set_grid(tmp_grid)

    def reset(self):
        new_grid = Grid()
        self.set_grid(new_grid.get_grid())

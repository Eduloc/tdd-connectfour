def line_win(grid):
    for lenght in range(6):
        for height in range(4):
            if grid.get_grid()[lenght * 7 + height] == "O" and \
                grid.get_grid()[lenght * 7 + height + 1] == "O" and \
                grid.get_grid()[lenght * 7 + height + 2] == "O" and \
                grid.get_grid()[lenght * 7 + height + 3] == "O":
                return True
    return False

def column_win(grid):
    for length in range(3):
        for height in range(7):
            if grid.get_grid()[length * 7 + height] == "O" and \
                grid.get_grid()[(length + 1) * 7 + height] == "O" and \
                grid.get_grid()[(length + 2) * 7 + height] == "O" and \
                grid.get_grid()[(length + 3) * 7 + height] == "O":
                return True
    return false
def diagonal_right_win(grid):
    for length in range(3):
        for height in range(4):
            if grid.get_grid()[length * 7 + height] == 'O' and \
                grid.get_grid()[(length + 1) * 7 + height + 1] == 'O' and \
                grid.get_grid()[(length + 2) * 7 + height + 2] == 'O' and \
                grid.get_grid()[(length + 3) * 7 + height + 3] == 'O' and \
                grid.get_grid()[(length + 4) * 7 + height + 4] == 'O':
                return True
    return False

def diagonal_left_win(grid):
    for length in range(3):
        for height in range(6, 3, -1):
            if grid.get_grid()[length * 7 + height] == 'O' and \
                grid.get_grid()[(length + 1) * 7 + height - 1] == 'O' and \
                grid.get_grid()[(length + 2) * 7 + height - 2] == 'O' and \
                grid.get_grid()[(length + 3) * 7 + height - 3] == 'O' and \
                grid.get_grid()[(length + 4) * 7 + height - 4] == 'O':
                return True
    return False
